def download_from_url(url, dest, **kwargs):
    import os
    import sys
    import urllib.request

    force = kwargs.get("force", False)
    verbose = kwargs.get("verbose", True)
    if verbose:
        print(f"Downloading \n{url}\n -> {dest}\n")
    # when *force* is False, only proceed with the download
    # if path *dest* doesn't exist already
    if force or not os.path.exists(dest):
        try:
            urllib.request.urlretrieve(url, dest)
            return dest
        except Exception as e:
            error_msg = f"Download failed for {url}\n{repr(e)}"
            if verbose:
                sys.stderr.write(error_msg)
            return error_msg
    else:
        return dest

def ensure_dir_exists(path):
    import os
    import sys

    if not os.path.exists(path):
        os.makedirs(path)
        
def tofile(dest, text, **kwargs):
    import sys

    verbose = kwargs.get("verbose", True)
    if verbose:
        print(f"Creating file {dest}")
    try:
        with open(dest, "w") as f:
            f.write(text)
            return dest
    except Exception as e:
        error_msg = f"Error writing file {dest}\n{repr(e)}"
        if verbose:
            sys.stderr.write(error_msg)
        return error_msg
