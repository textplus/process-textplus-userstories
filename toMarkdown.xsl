<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math" xmlns:local="local"
    xmlns:json="http://www.w3.org/2005/xpath-functions"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="#all" version="3.0">

    <xsl:output method="text"/>

    <!-- **** [ imports, includes ... **** -->

    <!-- **** ... imports, includes ] **** -->


    <!-- **** [ global params and vars ... **** -->
    <xsl:variable name="NEWLINE" select="'&#x0A;'"/>
    <!--<xsl:variable name="BASEDIR" select="local:dir-from-location(base-uri())"/>-->
    <xsl:param name="BASEDIR"/>
    <!-- **** ... global params and vars ] **** -->

    <xsl:key name="doc-by-filename" match="div[@data-file]" use="@data-file"/>

    <!-- **** [ templates ... **** -->
    <xsl:template match="node() | @*">
        <!--<xsl:copy>-->
        <xsl:apply-templates select="node()"/>
        <!--</xsl:copy>-->
    </xsl:template>

    <xsl:template match="text()" priority="100">
        <xsl:value-of select="replace(., '\p{Z}', ' ')"/>
    </xsl:template>

    <xsl:template match="/">
        <doc>
            <!--<info>
                <xsl:for-each select="distinct-values(//*/name())">
                    <xsl:sort/>
                    <elem>
                        <xsl:value-of select="."/>
                    </elem>
                </xsl:for-each>
            </info>-->
            <xsl:variable name="tables" select="//table"/>
            <xsl:variable name="imgs" select="//figure/noscript/img"/>
            <xsl:variable name="lists" select="//ul | //ol"/>
            <!--<xsl:copy-of select="$tables"/>-->
            <!--<div><xsl:value-of select="count($imgs)"/> images</div>-->
            <xsl:result-document href="{$BASEDIR}/download_info.json" method="text">
                <xsl:text>[</xsl:text>
                <xsl:for-each select="$imgs">
                    <xsl:variable name="titleNorm"
                        select="local:get-titleNorm(key('doc-by-filename', ancestor::div[@data-file]/@data-file))"/>
                    <xsl:variable name="imginfo" select="local:img-info(., $titleNorm)"/>
                    <xsl:text>
{ "download_src": "</xsl:text><xsl:value-of select="@src"/>",
                        <xsl:text>"download_dest": "</xsl:text><xsl:value-of
                        select="$imginfo/@downloadto"/>",
                        <xsl:text>"parent_file": "</xsl:text><xsl:value-of
                        select="$imginfo/@userstoryFile"/>" } <xsl:if
                        test="not(position() eq last())">,</xsl:if>
                </xsl:for-each>
                <xsl:text>]</xsl:text>
            </xsl:result-document>

            <!--<xsl:apply-templates select="$tables"/>-->
            <!--<xsl:message select="static-base-uri()"/>
            <xsl:message select="base-uri()"/>
            <xsl:message select="local:dir-from-location(base-uri())"/>-->
            <xsl:apply-templates/>
        </doc>
    </xsl:template>

    <xsl:template match="div[@data-file]">
        <xsl:variable name="title" select="local:trim(descendant::h3[1])"/>
        <xsl:variable name="titleNorm" select="local:get-titleNorm(.)"/>
        <xsl:variable name="transformed">
            <xsl:text>---
</xsl:text>
            <xsl:sequence select="
                    local:metadata-item('title',
                    local:encode-json-string($title), false())"/>
            <xsl:sequence select="local:metadata-item('type', 'user-story', false())"/>
            <xsl:sequence
                select="local:metadata-item('slug', 'user-story-' || local:user-story-nr(@data-file), false())"/>
            <xsl:variable name="dfg_areas" as="xs:string *">
                <xsl:analyze-string
                    select="local:trim(substring-after(descendant::p[starts-with(local:trim(.), 'DFG subject area')], ':'))"
                    regex="\d+([–\-]\d+)?[^\d]+">
                    <xsl:matching-substring>
                        <xsl:value-of select="replace(., ',\s*$', '')"/>
                    </xsl:matching-substring>
                    <xsl:non-matching-substring>
                        <xsl:if test="matches(., '\w')"><xsl:value-of select="."/></xsl:if>
                    </xsl:non-matching-substring>
                </xsl:analyze-string>
            </xsl:variable>

            <xsl:sequence select="
                    local:metadata-item('dfg_areas', $dfg_areas, true())"/>
            <xsl:variable name="domains" select="
                    for $x in descendant::p[contains(local:trim(a[1]), 'Text+ data domain')]
                    return
                        local:trim(substring-after($x, ':'))"/>
            <xsl:sequence select="
                    local:metadata-item('text_plus_domains', local:extract-domains($domains), true())"/>
            <xsl:variable name="authors-text" select="local:trim(descendant::h3[1]/following-sibling::p[1])"/>
            <xsl:variable name="authors" as="xs:string *">
                <xsl:analyze-string select="$authors-text" regex="[\w\s\-\.&amp;&quot;“”]+(\(.+?\))*">
                    <xsl:matching-substring>
                        <xsl:value-of select="normalize-space(.)"/>
                    </xsl:matching-substring>                    
                </xsl:analyze-string>
            </xsl:variable>
            <xsl:sequence select="
                    local:metadata-item('authors', $authors, true())"/>
            <xsl:text>---
</xsl:text>
            <xsl:apply-templates>
                <xsl:with-param name="titleNorm" tunnel="true" select="$titleNorm"/>
            </xsl:apply-templates>
            <xsl:text>

</xsl:text>
        </xsl:variable>
        <xsl:sequence select="$transformed"/>
        <xsl:result-document
            href="{$BASEDIR}/userstories-md/{local:user-story-dirname(@data-file, $titleNorm)}/index.md">
            <xsl:sequence select="$transformed"/>
        </xsl:result-document>
    </xsl:template>

    <xsl:template
        match="div[@class eq 'entry-content']/node()[following-sibling::h4[contains(., 'Motivation')]]">
        <!-- remove -->
    </xsl:template>

    <xsl:template match="noscript/img" priority="100">
        <xsl:param name="titleNorm" tunnel="true"/>
        <xsl:text> ![</xsl:text>
        <xsl:value-of select="@alt"/>
        <xsl:text>](</xsl:text>
        <xsl:value-of
            select="local:basename-from-location(local:img-info(., $titleNorm)/@downloadto)"/>
        <xsl:text>) </xsl:text>
    </xsl:template>

    <xsl:template match="img"><!--remove--></xsl:template>
    <xsl:template match="figcaption">
        <xsl:value-of select="'*' || local:trim(.) || '*'"/>
    </xsl:template>


    <xsl:template match="h1 | h2 | h3 | h4 | h5 | h6">
        <xsl:variable name="level" select="xs:integer(substring-after(name(), 'h'))"/>
        <xsl:text>

</xsl:text>
        <xsl:value-of select="
                string-join(for $item in (1 to $level)
                return
                    '#', '')"/>
        <xsl:text> </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>
</xsl:text>
    </xsl:template>

    <xsl:template match="div | p">
        <xsl:text>

</xsl:text>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="br">
        <xsl:text>  
</xsl:text>
    </xsl:template>

    <xsl:template match="table">
        <xsl:text>

</xsl:text>
        <xsl:for-each select="descendant::tr">
            <xsl:for-each select="td">|<xsl:value-of select="."/>
                <xsl:if test="position() eq last()">|<xsl:value-of select="$NEWLINE"/></xsl:if>
            </xsl:for-each>
            <xsl:if test="position() eq 1">
                <!-- separator line after the table head -->
                <xsl:for-each select="td">|---<xsl:if test="position() eq last()"
                    >|&#x0A;</xsl:if></xsl:for-each>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="ul | ol">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="ul/li"><xsl:text>
</xsl:text>* <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="ol/li"><xsl:text>
</xsl:text>
        <xsl:variable name="nr" select="
                if ((count(../li) eq 1) and ../@start) then
                    ../@start
                else
                    count(preceding-sibling::li) + 1"/>
        <!--<xsl:number count="ol/li" level="single"/>-->
        <xsl:value-of select="$nr"/>.<xsl:text> </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="a[@href]"> [<xsl:apply-templates/>](<xsl:value-of select="@href"
        />)</xsl:template>

    <xsl:template match="strong | b">
        <xsl:variable name="result">
            <xsl:apply-templates/>
        </xsl:variable>
        <xsl:variable name="markup" select="
                if (not(parent::*/name() = ('b', 'strong', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'))) then
                    '**'
                else
                    ''"/>
        <xsl:if test="local:trim($result)">
            <xsl:value-of select="$markup || local:trim($result) || $markup"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="em">
        <xsl:variable name="result">
            <xsl:apply-templates/>
        </xsl:variable>
        <xsl:if test="local:trim($result)">
            <xsl:text>*</xsl:text>
            <xsl:value-of select="local:trim($result)"/>
            <xsl:text>*</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="hr">
        <!-- remove -->
        <!--<xsl:text>

</xsl:text>-\-\-<xsl:text>

</xsl:text>-->
    </xsl:template>

    <xsl:template match="sup">
        <xsl:text>^</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>^</xsl:text>
    </xsl:template>

    <xsl:template match="sub">
        <xsl:text>~</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>~</xsl:text>
    </xsl:template>



    <!-- **** ... templates ] **** -->


    <!-- **** [ functions ... **** -->

    <xsl:function name="local:metadata-item">
        <xsl:param name="name" as="xs:string"/>
        <xsl:param name="values" as="xs:string *"/>
        <xsl:param name="make-list" as="xs:boolean"/>

        <xsl:value-of select="$name"/>
        <xsl:text>:</xsl:text>
        <xsl:for-each select="$values">
            <xsl:variable name="markup" select="
                    if ($make-list) then
                        $NEWLINE || '- '
                    else
                        ' '"/>
            <xsl:value-of select="$markup || ."/>
        </xsl:for-each>
        <xsl:text>

</xsl:text>
    </xsl:function>

    <xsl:function name="local:trim">
        <xsl:param name="str"/>
        <xsl:value-of select="normalize-space(replace($str, '\p{Z}', ' '))"/>
    </xsl:function>

    <xsl:function name="local:encode-json-string">
        <xsl:param name="input-string" as="xs:string"/>
        <xsl:variable name="input-string-elem" as="element(json:string)">
            <json:string>
                <xsl:value-of select="$input-string"/>
            </json:string>
        </xsl:variable>
        <xsl:value-of select="json:xml-to-json($input-string-elem, map {'indent': true()})"/>
    </xsl:function>

    <xsl:function name="local:user-story-nr">
        <xsl:param name="loc"/>
        <xsl:value-of select="
                replace(
                replace($loc, '\.html', ''), '.*user-story-', ''
                )"/>
    </xsl:function>

    <xsl:function name="local:user-story-dirname">
        <xsl:param name="userstoryFile"/>
        <xsl:param name="titleNorm"/>
        <xsl:value-of select="
                local:user-story-nr($userstoryFile)
                || '_' || $titleNorm"/>
    </xsl:function>

    <xsl:function name="local:dir-from-location">
        <xsl:param name="loc"/>
        <xsl:value-of select="string-join(tokenize($loc, '/')[position() ne last()], '/')"/>
    </xsl:function>

    <xsl:function name="local:basename-from-location">
        <xsl:param name="loc"/>
        <xsl:value-of select="tokenize(replace($loc, '/$', ''), '/')[position() eq last()]"/>
    </xsl:function>

    <xsl:function name="local:img-info" as="element(info)">
        <xsl:param name="img"/>
        <xsl:param name="titleNorm"/>
        <xsl:variable name="imgname" select="tokenize($img/@src, '/')[last()]"/>
        <xsl:variable name="userstoryFile"
            select="tokenize($img/ancestor::div[@data-file]/@data-file, '/')[last()]"/>
        <info imgname="{$imgname}" userstoryFile="{$userstoryFile}"
            downloadto="userstories-md/{local:user-story-dirname($userstoryFile, $titleNorm) || '/' || $imgname}"
        />
    </xsl:function>

    <xsl:function name="local:get-titleNorm">
        <xsl:param name="doc-div"/>
        <xsl:variable name="title" select="local:trim($doc-div/descendant::h3[1])"/>
        <xsl:value-of select="replace(replace($title, '\s+', '_'), '[^\w_]', '')"/>
    </xsl:function>

    <xsl:function name="local:extract-domains" as="xs:string *">
        <xsl:param name="items"/>
        <xsl:variable name="domains" as="xs:string *">
            <xsl:for-each select="$items">
                <xsl:variable name="item" select="."/>
                <xsl:for-each
                    select="('Collections', 'Editions', 'Lexical Resources', 'Infrastructure/Operations', 'Comprehensive')">
                    <xsl:if test="contains(lower-case($item), lower-case(.))">
                        <xsl:value-of select="."/>
                    </xsl:if>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:variable>
        <xsl:sequence select="
                if (count($domains) gt 0) then
                    $domains
                else
                    $items"/>
    </xsl:function>

    <!-- **** ... functions ] **** -->

</xsl:stylesheet>
