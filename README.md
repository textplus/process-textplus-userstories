# Downloading, processing and using the Text+ User Stories

## How to use

* Download this repo (or get it with `git clone`)
* To understand or recreate the generated data, read (and optionally run) the notebooks (the .ipynb files)
* Use the data in the **data** folder as needed
