---
title: "Etymological Dictionary of Medieval German (EWHSD)"

type: user-story

slug: user-story-515

dfg_areas:
- 104 Linguistics

text_plus_domains:
- Lexical Resources

authors:
- Rosemarie Lühr (Humboldt-Universität Berlin)
- Wolfgang Beck (Universität Jena)
- Julia Kuhn (Universität Jena)
- Ingrid Schröder (Universität Hamburg)

---




#### Motivation


Starting point: An etymological dictionary of German based on the latest developments in German medieval, Indo-German and Romance studies and current data processing technology, does not exist. Etymology plays a major role in rendering word meanings and in improving and ensuring the understanding of texts. A dictionary, focussing on the historical German vocabulary is an urgent need. This research desideratum is supposed to be eradicated by the project ‘Etymological Dictionary of Medieval German’ (EWHSD) (Rosemarie Lühr, Wolfgang Beck, Julia Kuhn, Ingrid Schröder). The EWHSD, which is to contain the entire vocabulary of the German-speaking Middle Ages (High German and Low German; native words and loan word), is digitally designed as a database and is to be published via a web platform. The dictionary will offer a wide range of research options and the opportunity of networking with other lexicographic resources, especially with the digital editions of the historical dictionaries.

**Objectives** 

Text+ could be involved in the development of tools during the project implementation and could expand the research possibilities after the end of the project by developing new tools.  In cooperation with the Academies of Sciences, the interfaces between the various lexicographic projects could be consolidated and developed in the long term. Text+ could enable appropriate networking of projects and resources and thereby offer the possibility to transfer visualization tools from other projects to our database. In addition, Text+ could create (easy to use) research opportunities for the public to serve knowledge transfer.

