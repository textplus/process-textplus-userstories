---
title: "Digital Klaus Groth Complete Edition"

type: user-story

slug: user-story-424

dfg_areas:
- 104 Linguistics
- 105 Literary Studies

text_plus_domains:
- Editions

authors:
- Robert Langhanke (Europa-Universität Flensburg, Klaus Groth Society)

---




#### Motivation


Since 1949, the Klaus Groth Society has been striving to promote the Low German and High German works of the poet Klaus Groth (1819-1899), who went down in literary history as a decisive innovator of the Neo-Low German literary language. As chairman of this society, I am involved in current processes which are aimed at the long-term backup of this work. Recently, the conviction has grown that a new, third complete edition of Groth’s texts has become necessary. This is due to the incompleteness of the previous editions, but also to the fact that their editorial execution is no longer up to date. In addition, the demands for commentary on the low-level texts have also changed in such a way that broader explanations of the language level have become necessary in order to enable all readers to participate in the texts. 

The unifying key to these ideas is the design of a complete digital edition, which makes versatile use of the possibilities of a dynamic modern text edition that flexibly meets different requirements. The background to this is the observation that the awareness of Groth’s work, which is by no means only of literary-historical significance, has been declining over the past decades, and that research activities have been declining as a result, leaving many questions unanswered. 

In order to establish a new basis and a starting point for corresponding efforts, many colleagues and I consider a digital complete edition to be indispensable. It also offers the opportunity to both use and test the options of digital text editions. 

Editorial philology plays a key role in the various fields of literary studies and linguistics, especially in individual language studies and historical linguistics. The development of digital edition formats currently determines the discourse. The continued connectivity of a literary work is also determined by its editorial status. 

#### Objectives


The motivation to create a digital complete edition of Groth’s works in order to ensure their continued general and academic reception, results in the objective of generating a tailor-made digital surface and structure for the necessary editorial needs. The benefits of the planned Complete Edition should be as diverse as possible, so that the digital presentation also requires greater openness. 

I imagine that different user groups would like to work with the texts. Therefore, it should be possible to achieve precisely fitting output formats of the text stock, to which a critical apparatus or a more comprehensive linguistic commentary could be optionally added. Close cooperation with the Schleswig-Holstein State Library, which holds the poet’s legacy, also makes it possible to link the text with original handwritten material and the appearance of the first editions. In this way, the texts are to be experienced on several levels and are not to be transformed into a singular new text edition by the new edition alone. Previous stages of work and philological development should remain visible and assessable. The lack of transparency of older grotesque text editions, which can sometimes be observed, is to be overcome in favor of an actual, recording disclosure of the genesis of the text stock. 

In addition to the edition of Low German and High German literary and factual texts by Groth, a long-term edition of the very extensive body of letters, which has so far only been partially catalogued, is to be produced. Letters from Groth as well as letters to Groth are to be made accessible in a database-supported network that will take into account and integrate editions already produced. The text volume of the planned edition project is thus versatile and can be evaluated via very different professional approaches. It represents an important personal corpus for the 19th century. 

#### Solutions


Digital editions have recently become a standard of editorial work. Accordingly, there are various reference editions, most of which are based on large projects. The Klaus Groth Complete Edition, which is currently in the planning stage, is based on smaller, but necessarily long-term structures, which are based on regional partnerships in Schleswig-Holstein and a versatile concept of third-party funding. 

For the concrete work steps of the philological editing and text entries to be commissioned, which will be preceded by comprehensive research processes of the poet’s heritage, barrier-free use by the general public must be accessible and differentiated in the applications. User-friendliness and data security as well as systemic longevity and robustness are the prerequisites for the technical basis of an edition project, which is to run for many years to make the first results accessible and retrievable at any time. 

#### Challenges


The primary challenge is marked by the texts themselves and their linguistic form. As the reformer of Neo-Low German literary linguistics, Groth made a decisive contribution to the grammatical, lexical and stylistic development of this linguistic form and, together with Karl Müllenhoff, who also supported the grammatical editing, he also developed a differentiated proposal for Low German orthography. This linguistic status of the Groth texts must be critically examined and documented. In older research, grammatically oriented descriptions of Groth’s literary language have provided a good basis for this. 

A further challenge is the design of the described digital surface and depth structure, which can meet the different requirements of text input and text output as accurately as possible and at the same time flexibly. This requires the design of a tailored database architecture that succeeds in linking philological and text-related aspects with technical solutions and implementation procedures. 

#### Review by Community


Steps towards an evaluation of the editorial project in the context of further editions in the Text+ network are a necessary test for the practicability and connectivity of the text presentations developed. The insight gained through Text+ into the very different forms and practices of the edition, which I also see taken up in varying degrees in the Digital Klaus Groth Edition, will help to adjust and define technical interfaces. Due to its smaller structures, primarily based on a literary society, the project is also particularly dependent on the outlined exchange. The project itself would like to introduce the proposal of an innovative and sustainable digital edition of Low German and other dialect literary texts into the discourse. 

